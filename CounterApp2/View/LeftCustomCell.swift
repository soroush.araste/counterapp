//
//  LeftCustomCell.swift
//  CounterApp2
//
//  Created by soroush amini araste on 3/24/20.
//  Copyright © 2020 soroush amini araste. All rights reserved.
//

import UIKit
import CoreData

class LeftCustomCell: UITableViewCell {
    
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var cellValueLabel: UILabel!
    @IBOutlet weak var minusBtnOutlet: UIButton!
    @IBOutlet weak var plusBtnOutlet: UIButton!
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var tempObj: CellValueModel?
    
    
    func setRecords(records: CellValueModel) {
        titleLabel.text = records.title
        cellValueLabel.text = "\(records.value)"
        saveContext()
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
        minusBtnOutlet.layer.cornerRadius = 5
        plusBtnOutlet.layer.cornerRadius = 5 
    }
    //tempObj will come from left view VC when create this cell
    @IBAction func minusBtnPressed(_ sender: UIButton!) {
        if tempObj != nil {
            tempObj?.value -= 1
            setRecords(records: tempObj!)
        }
        
    }
    @IBAction func plusBtnPressed(_ sender: UIButton!) {
        if tempObj != nil {
            tempObj?.value += 1
            setRecords(records: tempObj!)
        }
    }
    func saveContext() {
        do {
            try context.save()
        } catch  {
            print("error while trying to save data : \(error)")
        }
    }
}
