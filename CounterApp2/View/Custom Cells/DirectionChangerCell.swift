//
//  DirectionChangerCell.swift
//  CounterApp2
//
//  Created by soroush amini araste on 4/10/20.
//  Copyright © 2020 soroush amini araste. All rights reserved.
//

import UIKit
import CoreData

class DirectionChangerCell: UITableViewCell {

    @IBOutlet weak var directionChangerSegment: UISegmentedControl!
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var fastCounterSetArray: [CellValueModel]?
    
    var row: Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        loadData()
    }

    func cellUpdate(condition: Bool) {
        if condition == true {
            directionChangerSegment.selectedSegmentIndex = 0
        } else {
            directionChangerSegment.selectedSegmentIndex = 1
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func directionSegment(_ sender: UISegmentedControl) {
        switch directionChangerSegment.selectedSegmentIndex {
        case 0:
            fastCounterSetArray![row!].direction = true
        case 1:
            fastCounterSetArray![row!].direction = false
        default:
            print("error when try changing direction")
        }
        saveContext()
    }
    
    func saveContext() {
        do {
            try context.save()
        } catch  {
            print("error while trying to save data : \(error)")
        }
    }
    func loadData() {
        let request: NSFetchRequest<CellValueModel> = CellValueModel.fetchRequest()
        do {
            fastCounterSetArray = try context.fetch(request)
        } catch {
            print("error during load records : \(error)")
        }
    }

}
