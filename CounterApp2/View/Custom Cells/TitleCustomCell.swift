//
//  TitleCustomCell.swift
//  CounterApp2
//
//  Created by soroush amini araste on 4/9/20.
//  Copyright © 2020 soroush amini araste. All rights reserved.
//

import UIKit
import CoreData

class TitleCustomCell: UITableViewCell, UITextFieldDelegate {

    
    @IBOutlet var titleTextField: UITextField!
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var fastCounterSetArray: [CellValueModel]?
    var row: Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        loadData()
        // Initialization code
        titleTextField.clearButtonMode = .always
        titleTextField.spellCheckingType = .no
        titleTextField.autocorrectionType = .no
        titleTextField.delegate = self
        
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        fastCounterSetArray![row!].setValue(titleTextField.text, forKey: "title")
        saveContext()
    }
    func setTitle(row: Int) {
        titleTextField.text = fastCounterSetArray![row].title
    }
//    func rowNumberPassing(row: Int) {
//        titleTextField.text = fastCounterSetArray![row].title
//
//    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func saveContext() {
        do {
            try context.save()
        } catch  {
            print("error while trying to save data : \(error)")
        }
        
    }
    func loadData() {
        let request: NSFetchRequest<CellValueModel> = CellValueModel.fetchRequest()
        do {
            fastCounterSetArray = try context.fetch(request)
            //setTitle()
        } catch {
            print("error during load records : \(error)")
        }
    }
    
}
