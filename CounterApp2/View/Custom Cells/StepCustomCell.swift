//
//  StepCustomCell.swift
//  CounterApp2
//
//  Created by soroush amini araste on 4/9/20.
//  Copyright © 2020 soroush amini araste. All rights reserved.
//

import UIKit
import CoreData

class StepCustomCell: UITableViewCell {

    @IBOutlet weak var stepValueStepper: UIStepper!
    @IBOutlet weak var steperTextField: UITextField!
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var fastCounterSetArray: [CellValueModel]?
    var row: Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        loadData()
        steperTextField.inputView = UIView()
        steperTextField.inputAccessoryView = UIView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func updateCell(value: Int) {
        steperTextField.text = String(value)
        stepValueStepper.value = Double(value)
    }
    @IBAction func stepCellStepper(_ sender: UIStepper) {
        stepValueStepper.value = sender.value
        steperTextField.text = String(Int(stepValueStepper.value))
        fastCounterSetArray![row!].setValue(Int16(stepValueStepper.value), forKey: "step")
        saveContext()
    }
    
    
    func saveContext() {
        do {
            try context.save()
        } catch  {
            print("error while trying to save data : \(error)")
        }
        
    }
    func loadData() {
        let request: NSFetchRequest<CellValueModel> = CellValueModel.fetchRequest()
        do {
            fastCounterSetArray = try context.fetch(request)
            //setTitle()
        } catch {
            print("error during load records : \(error)")
        }
    }
}
