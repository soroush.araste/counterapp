//
//  CurrentValueCustomCell.swift
//  CounterApp2
//
//  Created by soroush amini araste on 4/9/20.
//  Copyright © 2020 soroush amini araste. All rights reserved.
//

import UIKit
import CoreData

class CurrentValueCustomCell: UITableViewCell {

    @IBOutlet weak var currentValueStepper: UIStepper!
    @IBOutlet weak var currentValueTextField: UITextField!
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var fastCounterSetArray: [CellValueModel]?
    var row: Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        loadData()
        currentValueTextField.inputView = UIView()
        currentValueTextField.inputAccessoryView = UIView()
    }

    func updateCell(value: Int) {
        currentValueTextField.text = String(value)
        currentValueStepper.value = Double(value)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func stepperPressed(_ sender: UIStepper) {
        currentValueStepper.value = sender.value
        currentValueTextField.text = String(Int(currentValueStepper.value))
        fastCounterSetArray![row!].setValue(Int16(currentValueStepper.value), forKey: "value")
        saveContext()
    }
    
    func saveContext() {
        do {
            try context.save()
        } catch  {
            print("error while trying to save data : \(error)")
        }
        
    }
    func loadData() {
        let request: NSFetchRequest<CellValueModel> = CellValueModel.fetchRequest()
        do {
            fastCounterSetArray = try context.fetch(request)
            //setTitle()
        } catch {
            print("error during load records : \(error)")
        }
    }

}
