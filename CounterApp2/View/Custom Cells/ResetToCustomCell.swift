//
//  ResetToCustomCell.swift
//  CounterApp2
//
//  Created by soroush amini araste on 4/9/20.
//  Copyright © 2020 soroush amini araste. All rights reserved.
//

import UIKit
import CoreData

class ResetToCustomCell: UITableViewCell {

    @IBOutlet weak var resetToStepper: UIStepper!
    @IBOutlet weak var resetToTextField: UITextField!
    
    var row: Int?
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var fastCounterSetArray: [CellValueModel]?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        loadData()
        resetToTextField.inputView = UIView()
        resetToTextField.inputAccessoryView = UIView()
    }

    func updateCell(value: Int) {
        resetToTextField.text = String(value)
        resetToStepper.value = Double(value)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func resetToStepper(_ sender: UIStepper) {
        resetToStepper.value = sender.value
        resetToTextField.text = String(Int(resetToStepper.value))
        fastCounterSetArray![row!].setValue(Int16(resetToStepper.value), forKey: "resetTo")
        saveContext()
    }
    
   func saveContext() {
       do {
           try context.save()
       } catch  {
           print("error while trying to save data : \(error)")
       }
   }
   func loadData() {
       let request: NSFetchRequest<CellValueModel> = CellValueModel.fetchRequest()
       do {
           fastCounterSetArray = try context.fetch(request)
       } catch {
           print("error during load records : \(error)")
       }
   }
    
}
