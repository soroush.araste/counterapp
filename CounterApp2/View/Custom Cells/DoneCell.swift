//
//  DoneCell.swift
//  CounterApp2
//
//  Created by soroush amini araste on 4/19/20.
//  Copyright © 2020 soroush amini araste. All rights reserved.
//

import UIKit

class DoneCell: UITableViewCell {

    @IBOutlet weak var doneBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func doneBtnAction(_ sender: Any) {
        let notif = Notification.Name(rawValue: saveButtonNotification)
        NotificationCenter.default.post(name: notif, object: nil)
    }
    
}
