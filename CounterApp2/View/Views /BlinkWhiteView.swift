//
//  BlinkWhiteView.swift
//  CounterApp2
//
//  Created by soroush amini araste on 4/3/20.
//  Copyright © 2020 soroush amini araste. All rights reserved.
//

import UIKit

class BlinkWhiteView: NSObject {
    
    let blinkerWhiteView = UIView()
    
    func animateWhiteView() {
        if let window = UIApplication.shared.keyWindow {
            blinkerWhiteView.backgroundColor = UIColor.white
            blinkerWhiteView.frame = window.frame
            window.addSubview(blinkerWhiteView)
            //blinkerWhiteView.frame = CGRect(x: 0, y: 0, width: window.frame.width, height: window.frame.height)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.15) {
                self.disapeareWhiteView()
            }
        }
    }
    
    func disapeareWhiteView() {
        blinkerWhiteView.removeFromSuperview()
    }
    override init() {
        super.init()
    }
}
