//
//  ShowBlackView.swift
//  CounterApp2
//
//  Created by soroush amini araste on 4/2/20.
//  Copyright © 2020 soroush amini araste. All rights reserved.
//

import UIKit


class SwipeUpMainScreen: NSObject {
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var fastCounterSetArray: [CellValueModel]?

    let resetNotif = Notification.Name(rawValue: resetNotification)
    let resetButtonUpdateUI = Notification.Name(rawValue: "resetButton")
    let animatedBlckView = UIView()
    
    var resetValue = 0
    var selectedRow: Int?
    
    func makeObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleResetValue), name: resetNotif, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(defaultTitleForResetBtn), name: resetButtonUpdateUI, object: nil)
    }
    
    // set title when app just lunched
    
    @objc func defaultTitleForResetBtn(notification: Notification) {
        let mainVC = notification.object as! ViewController
        if let safeMainVcResetTo = mainVC.fastCounterSetArray.last {
            resetToZero.setTitle("Reset to \(safeMainVcResetTo.resetTo)", for: .normal)
            resetValue = Int(safeMainVcResetTo.resetTo)
            selectedRow = mainVC.rowSelectedBefore
        } else {
            resetToZero.setTitle("Reset to 0", for: .normal)
            resetValue = 0
        }
    }
    
    //set title when save button at right vc pressed
    @objc func handleResetValue(notification: Notification) {
        let rightVC = notification.object as! RightTableViewController
        let test = rightVC.fastCounterSetArray[rightVC.rowNumberFromLeftVC!].resetTo
        resetValue = Int(test)
        resetToZero.setTitle("Reset to \(test)", for: .normal)
        resetToZero.addTarget(self, action: #selector(buttonClicked(sender:)), for: .touchUpInside)
        selectedRow = rightVC.rowNumberFromLeftVC
    }
    
    //when reset button pressed
    
    @objc func buttonClicked(sender: UIButton){
        loadData()
        //        print("button Clicked \(resetValue)")
        //        print("this is selected row \(selectedRow)")
        //        print("how many item in array \(fastCounterSetArray?.count)")
        fastCounterSetArray![selectedRow!].value = Int16(resetValue)
        saveContext()
        let updateUiNotif = Notification.Name(rawValue: "notifMainViewToUpdateUI")
        NotificationCenter.default.post(name: updateUiNotif, object: nil)
    }
    
    //MARK: Make buttons -> share-reset-sound...
    
    let bottomView: UIView = {
        let view = UIView()
        return view
    }()
    let shareBtn: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = UIColor.green
        btn.setTitle("share", for: .normal)
        btn.tintColor = .black
        btn.layer.cornerRadius = 5.0
        btn.clipsToBounds = true
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    let resetToZero: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = .green
        btn.setTitle("Reset to 0", for: .normal)
        btn.tintColor = .black
        btn.layer.cornerRadius = 5.0
        btn.clipsToBounds = true
        btn.translatesAutoresizingMaskIntoConstraints = false
        
        return btn
    }()
    
    let soundOn: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = .green
        btn.setTitle("S", for: .normal)
        btn.tintColor = .black
        btn.layer.cornerRadius = 5.0
        btn.clipsToBounds = true
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let blinkeOnOrOff: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = .green
        btn.setTitle("Volt", for: .normal)
        btn.tintColor = .black
        btn.layer.cornerRadius = 5.0
        btn.clipsToBounds = true
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    let aboutBtn: UIButton = {
        let btn = UIButton(type: .system)
        btn.backgroundColor = .green
        btn.setTitle("?", for: .normal)
        btn.tintColor = .black
        btn.layer.cornerRadius = 5.0
        btn.clipsToBounds = true
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    //MARK: when user swipe up the screen the background become black and new view with buttons come up
    
    func animatedBlackView() {
        if let window = UIApplication.shared.keyWindow {
            animatedBlckView.backgroundColor = UIColor(white: 0, alpha: 0.5)
            animatedBlckView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleDismiss)))
            window.addSubview(animatedBlckView)
            
            bottomView.addSubview(resetToZero)
            bottomView.addSubview(shareBtn)
            bottomView.addSubview(soundOn)
            bottomView.addSubview(blinkeOnOrOff)
            bottomView.addSubview(aboutBtn)
            resetToZero.addTarget(self, action: #selector(buttonClicked(sender:)), for: .touchUpInside)
            window.addSubview(bottomView)
            
            bottomView.backgroundColor = UIColor.white
            
            let height: CGFloat = window.frame.height / 3
            let y = window.frame.height - height
            
            bottomView.frame = CGRect(x: 0, y: window.frame.height, width: window.frame.width, height: height)
            animatedBlckView.frame = window.frame
            animatedBlckView.alpha = 0
            UIView.animate(withDuration: 0.2) {
                self.animatedBlckView.alpha = 1
                self.bottomView.frame = CGRect(x: 0, y: y, width: self.bottomView.frame.width, height: self.bottomView.frame.height)
            }
        }
        autoConstraints()
    }
    
    //if user clicked on black background view
    
    @objc func handleDismiss() {
        UIView.animate(withDuration: 0.2) {
            self.animatedBlckView.alpha = 0
            self.bottomView.removeFromSuperview()
        }
    }
    
    override init() {
        super.init()
        makeObserver()
        loadData()
    }
//MARK: Adding Constraints for buttons and swiped up views
    func autoConstraints() {
        
        let bottomViewWidth = bottomView.frame.width
        let bottomViewHeight = bottomView.frame.height
        let equalSpace = ((bottomViewWidth - 120) - (bottomViewWidth / 4)) / 3
        
        //Reset to Zero button
        resetToZero.leftAnchor.constraint(equalTo: bottomView.leftAnchor, constant: bottomViewWidth / 4).isActive = true
        resetToZero.topAnchor.constraint(equalTo: bottomView.topAnchor, constant: bottomViewHeight / 8).isActive = true
        resetToZero.widthAnchor.constraint(equalToConstant: 100).isActive = true
        resetToZero.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        // share button
        
        shareBtn.leftAnchor.constraint(equalTo: resetToZero.rightAnchor, constant: equalSpace).isActive = true
        shareBtn.centerYAnchor.constraint(equalTo: resetToZero.centerYAnchor).isActive = true
        shareBtn.widthAnchor.constraint(equalToConstant: 40).isActive = true
        shareBtn.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        //sound on/off
        
        soundOn.leftAnchor.constraint(equalTo: bottomView.leftAnchor, constant: bottomViewWidth / 4).isActive = true
        soundOn.bottomAnchor.constraint(equalTo: bottomView.bottomAnchor, constant: -30).isActive = true
        soundOn.widthAnchor.constraint(equalToConstant: 40).isActive = true
        soundOn.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        //Blink on or off
        
        blinkeOnOrOff.leftAnchor.constraint(equalTo: soundOn.rightAnchor, constant: equalSpace - 20).isActive = true
        blinkeOnOrOff.centerYAnchor.constraint(equalTo: soundOn.centerYAnchor).isActive = true
        blinkeOnOrOff.widthAnchor.constraint(equalToConstant: 40).isActive = true
        blinkeOnOrOff.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        // about button
        
        aboutBtn.leftAnchor.constraint(equalTo: blinkeOnOrOff.rightAnchor, constant: equalSpace - 20).isActive = true
        aboutBtn.centerYAnchor.constraint(equalTo: blinkeOnOrOff.centerYAnchor).isActive = true
        aboutBtn.widthAnchor.constraint(equalToConstant: 40).isActive = true
        aboutBtn.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }
    
    
}
