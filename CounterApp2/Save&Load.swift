//
//  Save&Load.swift
//  CounterApp2
//
//  Created by soroush amini araste on 4/27/20.
//  Copyright © 2020 soroush amini araste. All rights reserved.
//

import CoreData
import UIKit

extension ViewController {
    
    func saveContext() {
        do {
            try context.save()
        } catch  {
            print("error while trying to save data : \(error)")
        }
    }
    
    func loadData() {
        let request: NSFetchRequest<CellValueModel> = CellValueModel.fetchRequest()
        do {
            fastCounterSetArray = try context.fetch(request)
        } catch {
            print("error during load records : \(error)")
        }
    }
}

extension RightTableViewController {
    
    func saveContext() {
        do {
            try context.save()
        } catch  {
            print("error while trying to save data : \(error)")
        }
    }
    
    func loadData() {
        let request: NSFetchRequest<CellValueModel> = CellValueModel.fetchRequest()
        do {
            fastCounterSetArray = try context.fetch(request)
        } catch {
            print("error during load records : \(error)")
        }
    }
}

extension SwipeUpMainScreen{
    
    func saveContext() {
        do {
            try context.save()
        } catch  {
            print("error while trying to save data : \(error)")
        }
    }
    
    func loadData() {
        let request: NSFetchRequest<CellValueModel> = CellValueModel.fetchRequest()
        do {
            fastCounterSetArray = try context.fetch(request)
        } catch {
            print("error during load records : \(error)")
        }
    }
}
extension LeftViewController {
    func saveContext() {
        do {
            try context.save()
        } catch  {
            print("error while trying to save data : \(error)")
        }
    }
    
    func loadData() {
        let request: NSFetchRequest<CellValueModel> = CellValueModel.fetchRequest()
        do {
            fastCounterSetArray = try context.fetch(request)
        } catch {
            print("error during load records : \(error)")
        }
    }
}

