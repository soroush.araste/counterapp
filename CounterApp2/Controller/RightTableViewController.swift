//
//  RightTableViewController.swift
//  CounterApp2
//
//  Created by soroush amini araste on 3/21/20.
//  Copyright © 2020 soroush amini araste. All rights reserved.
//

import UIKit
import CoreData

class RightTableViewController: UITableViewController {
    
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var fastCounterSetArray = [CellValueModel]()
    
    //Section Header For TableView Rows
    let sectionHeadersArray = ["TITLE", "CURRENT VALUE", "STEP", "RESET TO", "DIRECTION", "INCLUDE"]
    
    var titleForEdit: String?
    var currentValue: Int?
    var stepValue: Int?
    var resetTo: Int?
    var direction: Bool?
    
    var rowNumberFromLeftVC: Int?
    
    
    let notif = Notification.Name(rawValue: saveButtonNotification)
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registeringCells()
        loadData()
        createObservers()
        preventEmptySettingScreen()
        
    }
    
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    //MARK: Cheking if any row number is passed to this view to have data
    
    func preventEmptySettingScreen() {
        if rowNumberFromLeftVC == nil {
            let newRecord = CellValueModel(context: context)
            newRecord.title = "untitled"
            newRecord.value = 0
            newRecord.resetTo = 0
            newRecord.step = 1
            newRecord.direction = true
            fastCounterSetArray.append(newRecord)
            saveContext()
            rowNumberFromLeftVC = 0
        }
    }
    
    // MARK: - Table view data source
    
    func registeringCells() {
        tableView.register(UINib(nibName: "TitleCell", bundle: nil), forCellReuseIdentifier: "titleCell")
        tableView.register(UINib(nibName: "CurrentValueCustomCell", bundle: nil), forCellReuseIdentifier: "currentValue")
        tableView.register(UINib(nibName: "StepCustomCell", bundle: nil), forCellReuseIdentifier: "stepCell")
        tableView.register(UINib(nibName: "ResetToCustomCell", bundle: nil), forCellReuseIdentifier: "resetToCell")
        tableView.register(UINib(nibName: "DirectionChangerCell", bundle: nil), forCellReuseIdentifier: "directionChanger")
        tableView.register(UINib(nibName: "IncludeCell", bundle: nil), forCellReuseIdentifier: "includeCell")
        tableView.register(UINib(nibName: "DoneCell", bundle: nil), forCellReuseIdentifier: "DoneCell")
    }
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionHeadersArray[section]
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return sectionHeadersArray.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 5 {
            return 2
        } else {
            return 1
        }
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 || indexPath.section == 4 || indexPath.section == 5 {
            return 50
        }
        return 70
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "titleCell", for: indexPath) as! TitleCustomCell
            cell.row = rowNumberFromLeftVC
            cell.setTitle(row: rowNumberFromLeftVC!)
            return cell
        } else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "currentValue", for: indexPath) as! CurrentValueCustomCell
            cell.updateCell(value: Int(fastCounterSetArray[rowNumberFromLeftVC!].value))
            cell.row = rowNumberFromLeftVC
            return cell
        } else if indexPath.section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "stepCell", for: indexPath) as! StepCustomCell
            //stepValue = Int(cell.steperTextField.text ?? "0")
            cell.updateCell(value: Int(fastCounterSetArray[rowNumberFromLeftVC!].step))
            cell.row = rowNumberFromLeftVC
            return cell
        } else if indexPath.section == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "resetToCell", for: indexPath) as! ResetToCustomCell
            cell.updateCell(value: Int(fastCounterSetArray[rowNumberFromLeftVC!].resetTo))
            cell.row = rowNumberFromLeftVC
            return cell
        }
        else if indexPath.section == 4 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "directionChanger", for: indexPath) as! DirectionChangerCell
            let condition = fastCounterSetArray[rowNumberFromLeftVC!].direction
            cell.cellUpdate(condition: condition)
            cell.row = rowNumberFromLeftVC
            return cell
        } else {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "includeCell", for: indexPath) as! IncludeCell
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "DoneCell", for: indexPath) as! DoneCell
                return cell
            }
            
        }
    }
    
    func createObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(RightTableViewController.changesInSettingIfSaveBtnPressed), name: notif, object: nil)
    }
    
    @objc func changesInSettingIfSaveBtnPressed()  {
        let name = Notification.Name(rawValue: titleNotification)
        NotificationCenter.default.post(name: name, object: nil)
        let resetNotificationRaw = Notification.Name(rawValue: resetNotification)
        NotificationCenter.default.post(name: resetNotificationRaw, object: self)
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: TableView Delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //changesInSettingSave()
        
    }
    
    
    
}

