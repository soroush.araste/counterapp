//
//  ViewController.swift
//  CounterApp2
//
//  Created by soroush amini araste on 3/20/20.
//  Copyright © 2020 soroush amini araste. All rights reserved.
//

import UIKit
import CoreData


let titleNotification = "CounterApp-TitleNotification"
let resetNotification = "CounterApp-ResetNotification"
let saveButtonNotification = "CounterApp-DoneButtonInSetting"
let notifyWhenDeleteRow = "CounterApp-DeletedRow"
let updateUiNotif = Notification.Name(rawValue: "notifMainViewToUpdateUI")

class ViewController: UIViewController {
    
    let leftTransition = LeftSlideTransition()
    let rightTransition = RightSlideTransition()
    let blackView = UIView()
    let animatedBlckView = UIView()
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var fastCounterSetArray = [CellValueModel]()
    var mainValueCenter: Int?
    var rowSelectedBefore: Int?
    
    var stepp: Int?
    var resetTo: Int?
    var direction: Bool?
    
    let updatedTitle = Notification.Name(rawValue: titleNotification)
    let resetButtonUpdateUI = Notification.Name(rawValue: "resetButton")
    let deletedRow = Notification.Name(rawValue: notifyWhenDeleteRow)
    
    @IBOutlet weak var titleOfRecord: UILabel!
    @IBOutlet weak var viewTapRecegnizor: UIView!
    @IBOutlet weak var timeAndDateLabel: UILabel!
    @IBOutlet weak var valueOfRecord: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dateAndTimeUpdateLabel()
        loadData()
        setupUI()
        createObservers()
        notif()
        //print(FileManager.default.urls(for: .documentDirectory, in: .userDomainMask))
    }
    override var prefersStatusBarHidden: Bool {
        return true
    }
    func createObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.updateUiBaseOnSettingChanges), name: updatedTitle, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.updateMainUI), name: deletedRow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateUiBaseOnSettingChanges), name: updateUiNotif, object: nil)
    }
    @objc func updateMainUI() {
        loadData()
        //print(fastCounterSetArray.last?.value)
        if let safeValue = fastCounterSetArray.last {
            valueOfRecord.text = "\(safeValue.value)"
            titleOfRecord.text = safeValue.title
            rowSelectedBefore = fastCounterSetArray.count - 1
            mainValueCenter = Int(safeValue.value)
            stepp = Int(safeValue.step)
            direction = safeValue.direction
        } else {
            let newRecord = CellValueModel(context: context)
            newRecord.title = "untitled"
            newRecord.value = 0
            newRecord.resetTo = 0
            newRecord.step = 1
            newRecord.direction = true
            fastCounterSetArray.append(newRecord)
            saveContext()
            
            mainValueCenter = 0
            rowSelectedBefore = 0
            valueOfRecord.text = "\(mainValueCenter ?? 0)"
            titleOfRecord.text = "untitled"
            stepp = 1
            direction = true
        }
        
    }
    
    @objc func updateUiBaseOnSettingChanges() {
        if let safeValue = rowSelectedBefore {
            titleOfRecord.text = fastCounterSetArray[safeValue].title
            mainValueCenter = Int(fastCounterSetArray[safeValue].value)
            valueOfRecord.text = String(fastCounterSetArray[safeValue].value)
            stepp = Int(fastCounterSetArray[safeValue].step)
            resetTo = Int(fastCounterSetArray[safeValue].resetTo)
            direction = fastCounterSetArray[safeValue].direction
        } else if let safeLastItem =  fastCounterSetArray.last {
            titleOfRecord.text = safeLastItem.title
            mainValueCenter = Int(safeLastItem.value)
            valueOfRecord.text = String(safeLastItem.value)
            stepp = Int(safeLastItem.step)
            resetTo = Int(safeLastItem.resetTo)
            direction = safeLastItem.direction
        }
    }
//MARK: send notification to update title of reset button(SwipeUp)
    
    func notif() {
        NotificationCenter.default.post(name: resetButtonUpdateUI, object: self)
    }
    
    @objc func setupUI() {
        
        if let safeValue = fastCounterSetArray.last {
            valueOfRecord.text = "\(safeValue.value)"
            titleOfRecord.text = safeValue.title
            rowSelectedBefore = fastCounterSetArray.count - 1
            mainValueCenter = Int(safeValue.value)
            stepp = Int(safeValue.step)
            direction = safeValue.direction
        } else {
            let newRecord = CellValueModel(context: context)
            newRecord.title = "untitled"
            newRecord.value = 0
            newRecord.resetTo = 0
            newRecord.step = 1
            newRecord.direction = true
            fastCounterSetArray.append(newRecord)
            saveContext()
            
           mainValueCenter = 0
            rowSelectedBefore = 0
            valueOfRecord.text = "\(mainValueCenter ?? 0)"
           titleOfRecord.text = "untitled"
            stepp = 1
           direction = true
        }
        
//MARK: ADD Gesture Recognizer For Main View
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(performSwipe(sender:)))
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(performSwipe(sender:)))
        swipeLeft.direction = .left
        
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(performSwipe(sender:)))
        swipeUp.direction = .up
        
        view.addGestureRecognizer(swipeRight)
        view.addGestureRecognizer(swipeLeft)
        view.addGestureRecognizer(swipeUp)
    }
    
    @objc func performSwipe(sender: UISwipeGestureRecognizer) {
        if sender.state == .ended {
            switch sender.direction {
            case .right:
                showLeftViewController()
            case .left:
                showRightViewController()
            case .up:
                downToUpRevealVC()
            default:
                break
            }
        }
    }
    
//MARK: Black View Behind The scene
    
    let showBlackView = SwipeUpMainScreen()
    
    func downToUpRevealVC() {
        showBlackView.animatedBlackView()
    }
    
//MARK: Pluse Button On The Main View Pressed
    
    @IBAction func addTallyBtnPressed(_ sender: UIButton) {
        showLeftViewController()
        saveContext()
    }
    
    func showLeftViewController() {
        let menuViewController = storyboard?.instantiateViewController(identifier: "menuViewController") as! LeftViewController
        menuViewController.rowSelectionDelegate = self
        menuViewController.modalPresentationStyle = .overCurrentContext
        menuViewController.transitioningDelegate = self
        present(menuViewController, animated: true)
    }
    
//MARK: Right View Show Button Pressed
    
    @IBAction func rightViewBtnPressed(_ sender: UIButton) {
        showRightViewController()
        saveContext()
    }
    func showRightViewController() {
     
         let rightViewController = storyboard?.instantiateViewController(identifier: "rightViewController") as! RightTableViewController
        rightViewController.rowNumberFromLeftVC = rowSelectedBefore
        rightViewController.modalPresentationStyle = .overCurrentContext
        rightViewController.transitioningDelegate = self
        present(rightViewController, animated: true)
    }
    
//MARK: make screen blink when screen tapped
    
    let tapForBlink = BlinkWhiteView()
    
    @IBAction func screenTapped(_ sender: UITapGestureRecognizer) {
        //print(direction)
        //print(mainValueCenter)
        //print(stepp)
        
        // If Direction == true The Value Goes Up
        
        if direction == true {
            mainValueCenter =  mainValueCenter! + stepp!
        } else {
            mainValueCenter =  mainValueCenter! - stepp!
        }
        
        tapForBlink.animateWhiteView()
        valueOfRecord.text = "\(mainValueCenter!)"
        fastCounterSetArray[rowSelectedBefore!].value = Int16(mainValueCenter!)
        saveContext()
        
    }
    
}
//MARK: Implement Right and Left View Controllers Delegate Methods

extension ViewController: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        if presented.title == "menuTitle" {
            leftTransition.isPresenting = true
            makeBlackView()
            return leftTransition
        } else {
            rightTransition.isPresenting = true
            makeBlackView()
            return rightTransition
        }
    }
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        if dismissed.title == "menuTitle" {
            leftTransition.isPresenting = false
            blackView.removeFromSuperview()
            return leftTransition
        } else {
            rightTransition.isPresenting = false
            blackView.removeFromSuperview()
            return rightTransition
        }
    }
    
}

extension ViewController {
    
//MARK: Date And Time Bottom of Main Screen
    
    func dateAndTimeUpdateLabel() {
        let currentDateTime = Date()
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        formatter.dateStyle = .short
        let dateTimeString = formatter.string(from: currentDateTime)
        timeAndDateLabel.text = "\(dateTimeString)"
    }
//MARK: make background black when left view or right view is presenting
    
    func makeBlackView() {
        
        blackView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        blackView.backgroundColor = .black
        blackView.alpha = 0.5
        view.addSubview(blackView)
    }
    
}

//MARK: handle updating data when one row selected in left View Controller

extension ViewController: RowOfRecordsSelected {
    func wichRowSeleced(rowNumber: Int) {
        loadData()
        rowSelectedBefore = rowNumber
        let shortFSA = fastCounterSetArray[rowNumber]
        titleOfRecord.text = shortFSA.title
        valueOfRecord.text = "\(shortFSA.value)"
        mainValueCenter = Int(shortFSA.value)
        stepp = Int(shortFSA.step)
        direction = shortFSA.direction
        resetTo = Int(shortFSA.resetTo)
    }
}
