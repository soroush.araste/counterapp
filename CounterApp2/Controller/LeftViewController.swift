//
//  LeftViewController.swift
//  CounterApp2
//
//  Created by soroush amini araste on 3/21/20.
//  Copyright © 2020 soroush amini araste. All rights reserved.
//

import UIKit


protocol RowOfRecordsSelected {
    func wichRowSeleced(rowNumber: Int)
}

class LeftViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var fastCounterSetArray = [CellValueModel]()
    
    var rowSelectionDelegate: RowOfRecordsSelected!
    
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadData()
        setupUI()
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        
    }
    @IBAction func addBtnPressed(_ sender: Any) {
        addNewRecord()
        loadData()
    }
    
    @IBAction func unlockBtnPressed(_ sender: Any) {
        
    }
    @IBAction func editBtnPressed(_ sender: Any) {
    }
    @IBAction func resetBtnPressed(_ sender: Any) {
        resetAllValues()
    }
    @IBAction func aboutBtnPressed(_ sender: Any) {
    }
    @IBAction func shareBtnPressed(_ sender: Any) {
    }
    
    //when ResetAll button pressed this code will set all value to 0 and then will save context
    
    func resetAllValues() {
        for item in fastCounterSetArray {
            item.value = 0
        }
        saveContext()
    }
    func addNewRecord() {
        let newRecord = CellValueModel(context: context)
        newRecord.title = "untitled"
        newRecord.value = 0
        newRecord.resetTo = 0
        newRecord.step = 1
        newRecord.direction = true
        fastCounterSetArray.append(newRecord)
        saveContext()
    }
    func setupUI() {
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(performSwipe(sender:)))
        swipeLeft.direction = .left
        view.addGestureRecognizer(swipeLeft)
    }
    @objc func performSwipe(sender: UISwipeGestureRecognizer) {
        if sender.state == .ended {
            switch sender.direction {
            case .left:
                dismiss(animated: true, completion: nil)
            default:
                break
            }
        }
    }
}

extension LeftViewController {
    
    // MARK: - Table view data source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return fastCounterSetArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.rowHeight = 100
        let records = fastCounterSetArray[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "leftMenuCell") as! LeftCustomCell
        //cell.minusBtnOutlet.tag = indexPath.row
        cell.setRecords(records: records)
        cell.tempObj = records
        return cell
    }
    //MARK: use delegation protocole to tell main view controller to updating ui based on which row selected
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        rowSelectionDelegate?.wichRowSeleced(rowNumber: indexPath.row)
        dismiss(animated: true, completion: nil)
    }
    //MARK: makes rows ready to edite -> delete
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            //print(fastCounterSetArray.count)
            context.delete(fastCounterSetArray[indexPath.row])
            fastCounterSetArray.remove(at: indexPath.row)
            
            //print(fastCounterSetArray.count)
            tableView.beginUpdates()
            tableView.deleteRows(at: [indexPath], with: .automatic)
            tableView.endUpdates()
            print("row \(indexPath.row) deleted")
            saveContext()
            notifyEveryOneRowDeleted()
        }
    }
    //send notification when a row deleted
    func notifyEveryOneRowDeleted() {
        let notify = Notification.Name(rawValue: notifyWhenDeleteRow )
        NotificationCenter.default.post(name: notify, object: nil)
    }
}
